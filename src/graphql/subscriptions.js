/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateVehicle = /* GraphQL */ `
  subscription OnCreateVehicle {
    onCreateVehicle {
      id
      name
      vehicle
      category
      vin
      licensePlate
      status
      devices {
        items {
          id
          deviceName
          phoneNumber
          carrier
          status
          notes
        }
        nextToken
      }
    }
  }
`;
export const onUpdateVehicle = /* GraphQL */ `
  subscription OnUpdateVehicle {
    onUpdateVehicle {
      id
      name
      vehicle
      category
      vin
      licensePlate
      status
      devices {
        items {
          id
          deviceName
          phoneNumber
          carrier
          status
          notes
        }
        nextToken
      }
    }
  }
`;
export const onDeleteVehicle = /* GraphQL */ `
  subscription OnDeleteVehicle {
    onDeleteVehicle {
      id
      name
      vehicle
      category
      vin
      licensePlate
      status
      devices {
        items {
          id
          deviceName
          phoneNumber
          carrier
          status
          notes
        }
        nextToken
      }
    }
  }
`;
export const onCreateStaff = /* GraphQL */ `
  subscription OnCreateStaff {
    onCreateStaff {
      id
      firstName
      lastName
      phone
      coachingOpportunity
      status
    }
  }
`;
export const onUpdateStaff = /* GraphQL */ `
  subscription OnUpdateStaff {
    onUpdateStaff {
      id
      firstName
      lastName
      phone
      coachingOpportunity
      status
    }
  }
`;
export const onDeleteStaff = /* GraphQL */ `
  subscription OnDeleteStaff {
    onDeleteStaff {
      id
      firstName
      lastName
      phone
      coachingOpportunity
      status
    }
  }
`;
export const onCreateDevice = /* GraphQL */ `
  subscription OnCreateDevice {
    onCreateDevice {
      id
      deviceName
      phoneNumber
      carrier
      vehicle {
        id
        name
        vehicle
        category
        vin
        licensePlate
        status
        devices {
          nextToken
        }
      }
      status
      notes
    }
  }
`;
export const onUpdateDevice = /* GraphQL */ `
  subscription OnUpdateDevice {
    onUpdateDevice {
      id
      deviceName
      phoneNumber
      carrier
      vehicle {
        id
        name
        vehicle
        category
        vin
        licensePlate
        status
        devices {
          nextToken
        }
      }
      status
      notes
    }
  }
`;
export const onDeleteDevice = /* GraphQL */ `
  subscription OnDeleteDevice {
    onDeleteDevice {
      id
      deviceName
      phoneNumber
      carrier
      vehicle {
        id
        name
        vehicle
        category
        vin
        licensePlate
        status
        devices {
          nextToken
        }
      }
      status
      notes
    }
  }
`;
