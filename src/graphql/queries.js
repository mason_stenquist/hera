/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getVehicle = /* GraphQL */ `
  query GetVehicle($id: ID!) {
    getVehicle(id: $id) {
      id
      name
      vehicle
      category
      vin
      licensePlate
      status
      devices {
        items {
          id
          deviceName
          phoneNumber
          carrier
          status
          notes
        }
        nextToken
      }
    }
  }
`;
export const listVehicles = /* GraphQL */ `
  query ListVehicles(
    $filter: ModelVehicleFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listVehicles(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        vehicle
        category
        vin
        licensePlate
        status
        devices {
          nextToken
        }
      }
      nextToken
    }
  }
`;
export const getStaff = /* GraphQL */ `
  query GetStaff($id: ID!) {
    getStaff(id: $id) {
      id
      firstName
      lastName
      phone
      coachingOpportunity
      status
    }
  }
`;
export const listStaffs = /* GraphQL */ `
  query ListStaffs(
    $filter: ModelStaffFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listStaffs(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        firstName
        lastName
        phone
        coachingOpportunity
        status
      }
      nextToken
    }
  }
`;
export const getDevice = /* GraphQL */ `
  query GetDevice($id: ID!) {
    getDevice(id: $id) {
      id
      deviceName
      phoneNumber
      carrier
      vehicle {
        id
        name
        vehicle
        category
        vin
        licensePlate
        status
        devices {
          nextToken
        }
      }
      status
      notes
    }
  }
`;
export const listDevices = /* GraphQL */ `
  query ListDevices(
    $filter: ModelDeviceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDevices(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        deviceName
        phoneNumber
        carrier
        vehicle {
          id
          name
          vehicle
          category
          vin
          licensePlate
          status
        }
        status
        notes
      }
      nextToken
    }
  }
`;
