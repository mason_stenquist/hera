/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createVehicle = /* GraphQL */ `
  mutation CreateVehicle(
    $input: CreateVehicleInput!
    $condition: ModelVehicleConditionInput
  ) {
    createVehicle(input: $input, condition: $condition) {
      id
      name
      vehicle
      category
      vin
      licensePlate
      status
      devices {
        items {
          id
          deviceName
          phoneNumber
          carrier
          status
          notes
        }
        nextToken
      }
    }
  }
`;
export const updateVehicle = /* GraphQL */ `
  mutation UpdateVehicle(
    $input: UpdateVehicleInput!
    $condition: ModelVehicleConditionInput
  ) {
    updateVehicle(input: $input, condition: $condition) {
      id
      name
      vehicle
      category
      vin
      licensePlate
      status
      devices {
        items {
          id
          deviceName
          phoneNumber
          carrier
          status
          notes
        }
        nextToken
      }
    }
  }
`;
export const deleteVehicle = /* GraphQL */ `
  mutation DeleteVehicle(
    $input: DeleteVehicleInput!
    $condition: ModelVehicleConditionInput
  ) {
    deleteVehicle(input: $input, condition: $condition) {
      id
      name
      vehicle
      category
      vin
      licensePlate
      status
      devices {
        items {
          id
          deviceName
          phoneNumber
          carrier
          status
          notes
        }
        nextToken
      }
    }
  }
`;
export const createStaff = /* GraphQL */ `
  mutation CreateStaff(
    $input: CreateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    createStaff(input: $input, condition: $condition) {
      id
      firstName
      lastName
      phone
      coachingOpportunity
      status
    }
  }
`;
export const updateStaff = /* GraphQL */ `
  mutation UpdateStaff(
    $input: UpdateStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    updateStaff(input: $input, condition: $condition) {
      id
      firstName
      lastName
      phone
      coachingOpportunity
      status
    }
  }
`;
export const deleteStaff = /* GraphQL */ `
  mutation DeleteStaff(
    $input: DeleteStaffInput!
    $condition: ModelStaffConditionInput
  ) {
    deleteStaff(input: $input, condition: $condition) {
      id
      firstName
      lastName
      phone
      coachingOpportunity
      status
    }
  }
`;
export const createDevice = /* GraphQL */ `
  mutation CreateDevice(
    $input: CreateDeviceInput!
    $condition: ModelDeviceConditionInput
  ) {
    createDevice(input: $input, condition: $condition) {
      id
      deviceName
      phoneNumber
      carrier
      vehicle {
        id
        name
        vehicle
        category
        vin
        licensePlate
        status
        devices {
          nextToken
        }
      }
      status
      notes
    }
  }
`;
export const updateDevice = /* GraphQL */ `
  mutation UpdateDevice(
    $input: UpdateDeviceInput!
    $condition: ModelDeviceConditionInput
  ) {
    updateDevice(input: $input, condition: $condition) {
      id
      deviceName
      phoneNumber
      carrier
      vehicle {
        id
        name
        vehicle
        category
        vin
        licensePlate
        status
        devices {
          nextToken
        }
      }
      status
      notes
    }
  }
`;
export const deleteDevice = /* GraphQL */ `
  mutation DeleteDevice(
    $input: DeleteDeviceInput!
    $condition: ModelDeviceConditionInput
  ) {
    deleteDevice(input: $input, condition: $condition) {
      id
      deviceName
      phoneNumber
      carrier
      vehicle {
        id
        name
        vehicle
        category
        vin
        licensePlate
        status
        devices {
          nextToken
        }
      }
      status
      notes
    }
  }
`;
