import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import  './assets/css/master.css'

import '@aws-amplify/ui-vue';
import Amplify, { API, graphqlOperation } from 'aws-amplify';
import aws_exports from './aws-exports';
import './plugins/element.js'

Amplify.configure(aws_exports);

//Register graphql api globaly (this.api)
Vue.mixin({
  methods: {
    api: (query, variables = {}) => API.graphql(graphqlOperation(query, variables))
  }
})

//Setup papa parse (this.$papa)
import VuePapaParse from 'vue-papa-parse'
Vue.use(VuePapaParse)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
