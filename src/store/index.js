import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userInfo: null,
    settingsView: 'SettingsNoSelection'
  },
  getters: {
    isLoggedIn: state => {
      return state.userInfo != null
    }
  },
  mutations: {
    setUserInfo(state, payload){
      state.userInfo =  payload
    },
    setSettingsView(state, view){
      state.settingsView = view
    }
  },
  actions: {
  },
  modules: {
  },
  plugins: [createPersistedState({
    paths: ['userInfo']
  })]
})
