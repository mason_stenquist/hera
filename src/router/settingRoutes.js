import Settings from '@/views/Settings/Settings'
import SettingsNoSelection from '@/views/Settings/SettingsNoSelection'
import CompanyDetails from '@/views/Settings/CompanyDetails'
import Devices from '@/views/Settings/Devices'
import Thresholds from '@/views/Settings/Thresholds'
import DropDowns from '@/views/Settings/DropDowns'

export default [
    {
      path: '',
      name: 'SettingsNoSelection',
      components: {
        default: Settings,
        settings: SettingsNoSelection
      }
    },
    {
      path: 'company',
      name: 'SettingsCompanyDetails',
      meta: { crumbTitle: 'Company Details'},
      components: {
        default: Settings,
        settings: CompanyDetails
      }
    },
    {
      path: 'devices',
      name: 'SettingsDevices',
      meta: { crumbTitle: 'Devices'},
      components: {
        default: Settings,
        settings: Devices
      }
    },{
      path: 'thresholds',
      name: 'SettingsThresholds',
      meta: { crumbTitle: 'Thresholds'},
      components: {
        default: Settings,
        settings: Thresholds
      }
    },{
      path: 'drop-downs',
      name: 'SettingsDropDowns',
      meta: { crumbTitle: 'Drop Down Lists'},
      components: {
        default: Settings,
        settings: DropDowns
      }
    }
  ]