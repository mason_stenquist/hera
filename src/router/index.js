import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/index.js'

import Settings from '@/views/Settings/Settings'
import settingRoutes from './settingRoutes.js'

Vue.use(VueRouter)



  const routes = [
  {
    path: '/',
    name: 'Login',
    meta: { layout: 'AuthLayout'},
    component: () => import(/* webpackChunkName: "login" */ '../views/Auth/Login.vue')
  },
  {
    path: '/reset',
    name: 'Reset',
    meta: { layout: 'AuthLayout'},
    component: () => import(/* webpackChunkName: "reset" */ '../views/Auth/Reset.vue')
  },
  {
    path: '/reset/confirm',
    name: 'ResetConfirm',
    props: true,
    meta: { layout: 'AuthLayout'},
    component: () => import(/* webpackChunkName: "resetConfirm" */ '../views/Auth/ResetConfirm.vue')
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    beforeEnter: requireAuth,
    component: () => import(/* webpackChunkName: "dashboard" */ '../views/Dashboard.vue')
  },
  {
    path: '/daily-roster',
    name: 'DailyRoster',
    beforeEnter: requireAuth,
    component: () => import(/* webpackChunkName: "dailyroster" */ '../views/DailyRoster.vue')
  },
  {
    path: '/staff',
    name: 'StaffIndex',
    beforeEnter: requireAuth,
    component: () => import(/* webpackChunkName: "staffindex" */ '../views/StaffIndex.vue')
  },
  {
    path: '/staff/import',
    name: 'StaffImport',
    beforeEnter: requireAuth,
    component: () => import(/* webpackChunkName: "staffimport" */ '../views/StaffImport.vue')
  },
  {
    path: '/staff/:id',
    name: 'StaffDetail',
    props: true,
    beforeEnter: requireAuth,
    component: () => import(/* webpackChunkName: "staffdetail" */ '../views/StaffDetail.vue')
  },
  {
    path: '/vehicles',
    name: 'VehicleIndex',
    beforeEnter: requireAuth,
    component: () => import(/* webpackChunkName: "vehicleindex" */ '../views/VehicleIndex.vue')
  },
  {
    path: '/vehicles/:id',
    name: 'VehicleDetail',
    props: true,
    beforeEnter: requireAuth,
    component: () => import(/* webpackChunkName: "vehicledetail" */ '../views/VehicleDetail.vue')
  },
  {
    path: '/settings',
    beforeEnter: requireAuth,
    component: Settings,
    children: settingRoutes 
  }
]




const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

function requireAuth(to, from, next){
  window.scrollTo(0, 0);
  if(!store.getters.isLoggedIn){
    next({
      path: '/',
      query: { redirect: to.fullPath }
    })
  }else{
    next()
  }
}


export default router
