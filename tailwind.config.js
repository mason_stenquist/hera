module.exports = {
  purge:{
    content: [
      './src/**/*.html', 
      './src/**/*.vue',
    ]
  },
  theme: {
    extend: {
      inset: {
        '-1': '-0.25rem',
        '-2': '-0.5rem',
        '-3': '-0.75rem',
        '-4': '-1rem',
        '-5': '-1.25rem',
        '-6': '-1.5rem',
        '-8': '-2rem',
        '-10': '-2.5rem',
        '-12': '-3rem',
        '-16': '-4rem'
      },
      spacing: {
        '72': '18rem',
        '84': '21rem',
        '96': '24rem',
        '108': '27rem',
      }
    },
  },
  variants: {},
  plugins: [],
}
